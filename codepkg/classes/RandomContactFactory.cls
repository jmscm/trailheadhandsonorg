public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer count, String lastName) {
        List<Contact> contactReturnList = new List<Contact>();
        for (Integer i = 1 ; i <= count ; i++) {
            contactReturnList.add(new Contact(FirstName='Test'+i,LastName=lastName));
        }
        return contactReturnList;
    }
}