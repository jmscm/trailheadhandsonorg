trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Task> taskList = new List<Task>();
    
    // Get the related task for the opportunities in this trigger
    Map<Id,Opportunity> oppsWithTasks = new Map<Id,Opportunity>(
        [SELECT Id,StageName,(SELECT Id FROM Tasks) FROM Opportunity WHERE Id IN :Trigger.New]);
    
    // Add an task for each opportunity if it doesn't already have one.
    // Iterate through each opportunity.
    for(Opportunity opp : Trigger.New) {
        System.debug('oppsWithTasks.get(opp.Id).Tasks.size()=' + oppsWithTasks.get(opp.Id).Tasks.size());
        // Check if the opportunity already has a related task.
        if (oppsWithTasks.get(opp.Id).Tasks.size() == 0 && oppsWithTasks.get(opp.Id).StageName == 'Closed Won') {
            // If it doesn't, add a default opportunity
            taskList.add(new Task(Subject = 'Follow Up Test Task', WhatId = oppsWithTasks.get(opp.Id).Id));
        }           
    }

    if (taskList.size() > 0) {
        insert taskList;
    }
}